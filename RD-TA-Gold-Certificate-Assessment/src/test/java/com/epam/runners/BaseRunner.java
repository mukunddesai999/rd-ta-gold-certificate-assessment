package com.epam.runners;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/features/", glue = "com.epam.stepdefs")
public class BaseRunner extends AbstractTestNGCucumberTests {

}