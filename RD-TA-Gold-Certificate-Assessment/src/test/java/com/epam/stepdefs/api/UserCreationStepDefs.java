package com.epam.stepdefs.api;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import java.util.Map;

public class UserCreationStepDefs {

    private Response response;
    @Given("a Base URI {string}")
    public void givenABaseURI(String baseURI) {
        RestAssured.baseURI = baseURI;
    }
    @When("I make a POST request with username and password to create user")
    public void iMakeAPOSTRequestWithUsernameAndPassword(DataTable dataTable) {
        Map<String,String> map = dataTable.asMap();
        String body = "{\n" +
                "  \"userName\": \""+map.get("username")+"\",\n" +
                "  \"password\": \""+map.get("password")+"\"\n" +
                "}";
        response = RestAssured.given()
                .body(body)
                .contentType(ContentType.JSON)
                .post();
    }
    @Then("the response code must be {int}")
    public void theResponseCodeMustBe(int statusCode) {
        Assert.assertEquals(response.statusCode(),statusCode);
    }
    @And("the response message should be {string}")
    public void theResponseMessageShouldBe(String errorMessage) {
        Assert.assertEquals(response.jsonPath().get("message"),errorMessage);
    }
    @And("username {string} should be verified")
    public void usernameShouldBeVerified(String username) {
        Assert.assertEquals(response.jsonPath().get("username"),username);
    }
    @DataTableType(replaceWithEmptyString = "[blank]")
    public String replace(String blank){
        return blank;
    }
}
