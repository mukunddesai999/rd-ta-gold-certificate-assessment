package com.epam.stepdefs.api;

import com.epam.framework.pages.BookCollection;
import com.epam.framework.pojos.Book;
import com.epam.framework.pojos.Books;
import com.epam.stepdefs.support.Hooks;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

import java.util.stream.Collectors;


public class GetBooksStepDefs {

    private BookCollection bookCollection;
    private Response response;
    private Books books;
    public GetBooksStepDefs(Hooks hooks){
        bookCollection = hooks.getComponent(BookCollection.class);
    }
    @When("I make a GET request to get all books")
    public void iMakeAGETRequestToGetAllBooks() {
        response = RestAssured.given()
                .get();
    }
    @And("I navigate to books page")
    public void iNavigateTo() {
        bookCollection.goToBooksPage();
    }
    @Then("I should get all books")
    public void iShouldGetAllBooks() {
        books = response.as(Books.class);
        Assert.assertTrue(bookCollection.getAllBookTitles()
                .containsAll(books.getBooks().stream().map(Book::getTitle).collect(Collectors.toList())));
    }

}
