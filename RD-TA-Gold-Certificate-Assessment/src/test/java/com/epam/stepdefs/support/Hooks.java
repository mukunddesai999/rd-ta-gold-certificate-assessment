package com.epam.stepdefs.support;

import com.epam.framework.pages.BasePage;
import com.epam.framework.pages.BookCollection;
import com.epam.framework.pages.HomePage;
import com.epam.framework.pages.LoginPage;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;

public class Hooks {
    MutablePicoContainer picoContainer = new DefaultPicoContainer();
    @Before
    public void addPages(){
        picoContainer.addComponent(LoginPage.class);
        picoContainer.addComponent(HomePage.class);
        picoContainer.addComponent(BookCollection.class);
    }
    public <T> T getComponent(Class<T> tClass){
        return picoContainer.getComponent(tClass);
    }
    @AfterAll
    public static void tearDown(){
        BasePage.teardown();
    }
}
