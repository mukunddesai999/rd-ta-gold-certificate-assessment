package com.epam.stepdefs.ui;

import com.epam.framework.pages.HomePage;
import com.epam.framework.pages.LoginPage;
import com.epam.stepdefs.support.Hooks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class LoginStepDefs {
    private LoginPage loginPage;
    private HomePage homePage;
    public LoginStepDefs(Hooks hooks){
        loginPage = hooks.getComponent(LoginPage.class);
        homePage = hooks.getComponent(HomePage.class);
    }
    @Given("I am on Login page")
    public void iAmOnLoginPage() {
        loginPage.goToLoginPage();
    }

    @When("I enter username {string} and password {string} and click on login")
    public void iEnterUsernameAndPasswordAndClickOnLogin(String username, String password) {
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.login();
    }

    @Then("my username {string} must be displayed on the page")
    public void myUsernameMustBeDisplayedOnThePage(String username) {
        Assert.assertEquals(homePage.getUserName(),username);
    }
}