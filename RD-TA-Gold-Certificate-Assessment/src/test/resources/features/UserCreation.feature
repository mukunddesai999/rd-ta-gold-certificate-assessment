Feature: This feature contains Scenarios to create new user

  @API
 Scenario Outline: This scenario is to create a user with blank credentials
    Given a Base URI "https://demoqa.com/Account/v1/User"
    When I make a POST request with username and password to create user
    |Key|Value|
    |username|<username>|
    |password|<password>|
    Then the response code must be 400
    And the response message should be "UserName and Password required."
   Examples:
   |username|password|
   |[blank]|[blank]|
   |mukund|[blank]|
   |[blank]|Assessment@123 |

  @API
  Scenario Outline: This scenario is to create new user with invalid password
    Given a Base URI "https://demoqa.com/Account/v1/User"
    When I make a POST request with username and password to create user
      |Key|Value|
      |username|<username>|
      |password|<password>|
    Then the response code must be 400
    And the response message should be "Passwords must have at least one non alphanumeric character, one digit ('0'-'9'), one uppercase ('A'-'Z'), one lowercase ('a'-'z'), one special character and Password must be eight characters or longer."
    Examples:
      |username|password|
      |mukund  |pass|
      |mukund  |pass@|
      |mukund  |pass@1|
      |mukund  |Pass@1|

    @API
    Scenario Outline: This scenario is to create a new user with valid credentials
      Given a Base URI "https://demoqa.com/Account/v1/User"
      When I make a POST request with username and password to create user
        |Key|Value|
        |username|<username>|
        |password|<password>|
      Then the response code must be 201
      And username "mukund1222" should be verified
      Examples:
      |username|password|
      |mukund1222|Pass@1234|