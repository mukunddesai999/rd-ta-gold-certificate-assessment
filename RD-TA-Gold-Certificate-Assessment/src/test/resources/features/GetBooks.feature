Feature: This feature is to get all the books in an account

  Scenario: This scenario is to get all the books in an account
    Given a Base URI "https://demoqa.com/BookStore/v1/Books"
    When I make a GET request to get all books
    And I navigate to books page
    Then I should get all books