package com.epam.framework.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class LoggerFactory {
    private static final Logger logger = LogManager.getRootLogger();

    private LoggerFactory(){

    }
    public static Logger getLogger() {
        return logger;
    }
}
