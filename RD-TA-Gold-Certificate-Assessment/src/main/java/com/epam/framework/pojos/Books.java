package com.epam.framework.pojos;

import lombok.Data;

import java.util.List;

@Data
public class Books {
    List<Book> books;
}
