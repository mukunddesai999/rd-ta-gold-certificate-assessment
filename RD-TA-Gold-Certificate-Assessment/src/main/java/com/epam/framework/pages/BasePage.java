package com.epam.framework.pages;

import com.epam.framework.constants.DriverType;
import com.epam.framework.managers.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class BasePage {
    protected static WebDriver driver;
    protected static WebDriverWait wait;
    protected BasePage(){
        if (driver == null){
            initializeDriver();
        }
    }
    protected static void initializeDriver(){
        driver = DriverManager.getDriver(DriverType.CHROME);
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }
    public static void teardown(){
        driver.quit();
    }
    protected void fillTextField(String value,By locator){
        driver.findElement(locator).sendKeys(value);
    }
    protected void goToPage(String link){
        driver.get(link);
    }
}
