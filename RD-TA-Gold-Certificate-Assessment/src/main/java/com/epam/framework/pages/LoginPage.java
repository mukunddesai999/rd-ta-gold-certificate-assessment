package com.epam.framework.pages;

import org.openqa.selenium.By;

import static com.epam.framework.constants.WebLinks.LOGIN_PAGE_URL;

public class LoginPage extends BasePage{
    private By usernameTextBox = By.id("userName");
    private By passwordTextBox = By.id("password");
    private By loginButton = By.id("login");

    public void goToLoginPage(){
        goToPage(LOGIN_PAGE_URL);
    }

    public void enterUsername(String username){
        fillTextField(username,usernameTextBox);
    }
    public void enterPassword(String password){
        fillTextField(password,passwordTextBox);
    }
    public void login(){
        driver.findElement(loginButton).click();
    }
}
