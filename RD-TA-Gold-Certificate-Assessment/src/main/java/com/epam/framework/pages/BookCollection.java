package com.epam.framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.framework.constants.WebLinks.BOOKS_PAGE_URL;

public class BookCollection extends BasePage{
    private By titles = By.className("mr-2");
    private By author = By.xpath("//div[text()=\"Author\"]");
    public void goToBooksPage(){
        goToPage(BOOKS_PAGE_URL);
    }
    public List<String> getAllBookTitles(){

        return driver.findElements(titles)
                .stream().map(WebElement::getText)
                .collect(Collectors.toList());
    }

}
