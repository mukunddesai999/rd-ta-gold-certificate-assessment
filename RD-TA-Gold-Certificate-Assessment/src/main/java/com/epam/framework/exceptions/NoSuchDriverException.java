package com.epam.framework.exceptions;

public class NoSuchDriverException extends RuntimeException{
    public NoSuchDriverException(String message){
        super(message);
    }
}
