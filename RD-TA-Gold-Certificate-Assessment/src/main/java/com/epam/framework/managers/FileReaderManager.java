package com.epam.framework.managers;

import com.epam.framework.logger.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileReaderManager {
    public Properties getFileReader(String filePath){
        Properties properties = new Properties();
        try(FileInputStream fis = new FileInputStream(filePath)){
            properties.load(fis);
        }catch (IOException e){
            LoggerFactory.getLogger().error("An IO exception Occurred ");
        }
        return properties;
    }
}
