package com.epam.framework.managers;

import com.epam.framework.constants.DriverType;
import com.epam.framework.drivers.MyChromeDriver;
import com.epam.framework.drivers.MyEdgeDriver;
import com.epam.framework.drivers.MyFirefoxDriver;
import com.epam.framework.exceptions.NoSuchDriverException;
import org.openqa.selenium.WebDriver;

public class DriverManager {
    private static WebDriver driver;
    private DriverManager(){

    }
    public static WebDriver getDriver(DriverType driverType){
        if (driver == null) {
            switch (driverType) {
                case CHROME:
                    driver = new MyChromeDriver().setup();
                    break;
                case EDGE:
                    driver = new MyEdgeDriver().setup();
                    break;
                case FIREFOX:
                    driver = new MyFirefoxDriver().setup();
                    break;
                default:
                    throw new NoSuchDriverException("No driver of type" + driverType + " found");
            }
        }
        return driver;
    }
}
