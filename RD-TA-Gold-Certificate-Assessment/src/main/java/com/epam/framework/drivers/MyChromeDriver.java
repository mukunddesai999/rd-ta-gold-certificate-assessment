package com.epam.framework.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyChromeDriver implements WebDriverFactory{
    @Override
    public WebDriver setup() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--allow-remote-origins=*");
        return new ChromeDriver(options);
    }
}
