package com.epam.framework.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class MyEdgeDriver implements WebDriverFactory{
    @Override
    public WebDriver setup() {
        EdgeOptions options = new EdgeOptions();
        options.addArguments("--allow-remote-origins=*");
        return new EdgeDriver(options);
    }
}
