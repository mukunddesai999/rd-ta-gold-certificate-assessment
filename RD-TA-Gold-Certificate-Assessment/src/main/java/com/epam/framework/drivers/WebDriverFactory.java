package com.epam.framework.drivers;

import org.openqa.selenium.WebDriver;

public interface WebDriverFactory {
    WebDriver setup();
}
