package com.epam.framework.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirefoxDriver implements WebDriverFactory{
    @Override
    public WebDriver setup() {
        return new FirefoxDriver();
    }
}
