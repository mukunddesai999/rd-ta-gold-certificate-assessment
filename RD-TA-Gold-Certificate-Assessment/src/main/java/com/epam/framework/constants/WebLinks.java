package com.epam.framework.constants;

public class WebLinks {
    private WebLinks(){

    }
    public static final String LOGIN_PAGE_URL = "https://demoqa.com/login";
    public static final String BOOKS_PAGE_URL = "https://demoqa.com/books";
}
