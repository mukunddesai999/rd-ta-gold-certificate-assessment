package com.epam.framework.constants;

public enum DriverType {
    CHROME,
    EDGE,
    FIREFOX
}
